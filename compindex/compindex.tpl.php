<?php
/**
 * @file
 *
 * Display the competition index.
 */
?>
<?php if (count($seasons) == 0): ?>
<p>There are no competition results.</p>
<?php else: ?>
<?php $curtype = 'none'; ?>
<?php foreach ($seasons as $season): ?>
<h2><?php print $season; ?></h2>
<ul>
<?php   $curtype = 'none'; ?>
<?php   foreach ($comps[$season] as $comp): ?>
<?php     if (($comp->entry_type_title) != $curtype
           && ($comp->entry_type_title != '')): /* POTY is '' */ ?>
<?php       if ($curtype != 'none'): ?>
        </ul>
      </li>
<?php       endif; ?>
      <li><span class="etype-title"><?php print $comp->entry_type_title; ?></span>
        <ul>
<?php       $curtype = $comp->entry_type_title; ?>
<?php     endif; ?>
          <li><?php print l($comp->title, 'node/' . $comp->nid); ?></li>
<?php   endforeach; ?>
<?php if ($curtype != 'none'): ?>
        </ul>
      </li>
<?php endif; ?>
</ul>
<?php endforeach;
    endif;