<?php
/**
 * @file
 * Keep track of scores in a super competition.
 */


/*
 * A number (usually two) of CumuEntrantScores which collects
 * cumulative competition results from more than one series of 
 * competitions and combines them into one set of results.
 */
class SuperEntrantScores {
  private $cumucomps = array();
  private $cumu_entrant_scores = array();
  private $author;
  public $position;
  
  /**
   * Construct a SuperEntrantScores object for an author.
   * 
   * @param $author
   * The author's name.
   */
  function __construct($author) {
    $this->author = $author;
  }

  /*
   * Add a set of scores for a cumulative competition for this author.
   * 
   * @param $cumucomp
   * The cumulative competition which is being added to. Of type CumuCompScores.
   * 
   * @param $cumu_entrant_scores
   * A CumuEntrantScores object containing this author's results from
   * a competition series.
   */
  function addCumuEntrantScores($cumucomp, $cumu_entrant_scores) {
    $this->cumucomps[] = $cumucomp;
    if ($cumu_entrant_scores->getAuthor() != $this->author) {
      throw new Exception("Invalid author, was " . $entrant_scores->getAuthor() . " should be " . $this->author);
    }
    $this->cumu_entrant_scores[$cumucomp->getCompetitionId()] = $cumu_entrant_scores;
  }

  /**
   * Get the author's name.
   */
  function getAuthor() {
    return $this->author;
  }

  /**
   * Safe version of author.
   */
  function getPlainAuthor() {
    return check_plain($this->author);
  }
  
  /*
   * Get the number of cumulative competitions which are being aggregated.
   */
  function getNumCumuComps() {
    return count($this->cumu_entrant_scores);
  }

  /*
   * The maximum number of competitions in the contained cumulative competitions.
   */
  function getMaxNumComps() {
    $result = 0;
    foreach ($this->cumu_entrant_scores as $cumu_entrant_scores) {
      $num = $cumu_entrant_scores->getNumEntrantScores();
      if ($num > $result) {
        $result = $num;
      }
    }
    return $result;
  }
  
  /*
   * Return the total of the cumulative competition scores.
   */
  function getTotal() {
    $result = 0;
    foreach ($this->cumu_entrant_scores as $cumu_entrant_scores) {
      $result += $cumu_entrant_scores->getTotal();
    }
    return $result;
  }

  /**
   * Get the user's id
   */
  function getUid() {
    if ($this->getNumCumuComps() > 0) {
      reset($this->cumu_entrant_scores);
      $entrant_scores = current($this->cumu_entrant_scores); 
      return $entrant_scores->getUid();
    }
    else {
      return -1;
    }
  }
  
  /**
   * Get the Cumulative Competition scores array.
   */
  function getCumuEntrantScores() {
    return $this->cumu_entrant_scores;
  }

  function getCumuComps() {
    return $this->cumucomps;
  }
  
  /**
   * Get the maximum number of scores in the competitions which make up
   * this accumulator.
   */
  function getMaxEntries() {
    $result = 0;
    foreach ($this->cumu_entrant_scores as $cumu_entrant_scores) {
      if ($cumu_entrant_scores->getMaxEntries() > $result) {
        $result = $cumu_entrant_scores->getMaxEntries();
      }
    }
    return $result;
  }
}
