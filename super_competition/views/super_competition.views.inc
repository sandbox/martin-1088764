<?php
/**
 * @file
 *
 * Views integration for super competitions.
 */

/**
 * Implements hook_views_data().
 */
function super_competition_views_data() {
  // Prefix in UI to make it easier to find the field.
  $data['super_competition']['table']['group'] = t('Competitions');

  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['super_competition']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['super_competition']['season'] = array(
    'title' => t('Super Competition Season'),
    'help' => t('The season the Super Competition took place.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Super Comp Season'),
    ),
  );

  $data['super_competition']['short_name'] = array(
    'title' => t('Short Name'),
    'help' => t('A short name for the Super Competition.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}