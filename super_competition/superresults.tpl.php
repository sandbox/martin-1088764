<?php
/**
 * @file
 * Display multiple cumulative competitions added together into a super competition.
 */
 ?>
<?php if ($num_comps == 0): ?>
<p>There are no results for this competition.</p>
<?php else: ?>
  <?php if ($scoring_scheme == 'top-nine'): ?>
  <p><em>Note: only the top nine scores are included in the total.</em></p>
  <?php endif; ?>
  <table class="scoresheet">
    <thead>
    <tr>
      <th>&#160;</th>
      <th>Name</th>
<?php
      for ($compno = 0; $compno < $num_comps; $compno++): ?>
        <th class="compno" colspan="<?php print $comp_info['max'][$compno] + 1 ?>"><?php print $compno + 1 ?></th>
      <?php endfor;?>
      <th>Total</th>
    </tr>
    </thead>
    <tbody>
<?php

  foreach ($super_entrant_scores as $super_entrant_score):
    if ($super_entrant_score->getNumCumuComps() <= 1) {
      continue;
    }
    $cumu_entrant_scores = $super_entrant_score->getCumuEntrantScores();
    $row = 0;
    foreach ($cumu_entrant_scores as $cumu_entrant_score):
      $entrant_scores = $cumu_entrant_score->getEntrantScores();
      $comp_no = 0;
      if ($row == 0):
  ?>
    <tr>
      <td><?php print $super_entrant_score->position ?></td>
      <td><?php print $super_entrant_score->getPlainAuthor() ?></td>
<?php
      else:
   ?>
    <tr>
      <td>&#160;</td>
      <td>&#160;</td>
<?php
      endif;
      foreach ($entrant_scores as $entrant_score) {
        $entry_no = 0;
        do {
          // Only competitions which have been entered are in the $entrant_scores
          // array, if the entrant didn't enter one this will skip through the
          // columns until it finds the correct column for this set of scores.
          $compid = $entrant_score->getCompetitionId();
          if ($compid != $comp_ids[$row][$comp_no]) {
            // Not his one, so fill in with blanks.
            for ($i = 0; $i <= $max_entries[$comp_no]; $i++) {
              ?><td>&#160;</td><?php
            }
            $comp_no++;
          }
        } while (($compid != $comp_ids[$row][$comp_no]) && ($comp_no < count($max_entries))); // Found its column or run out of columns.
        if ($compid == $comp_ids[$row][$comp_no]) { // Check again just to be sure, might have gone off the end of the columns.
          foreach ($entrant_score->getScores() as $score) {
            $entry_no++; 
            ?><td class="<?php print $score->getClass() ?>" title="<?php print $score->getPlainTitle ?>"><?php print $score->getPlainScore(); ?></td><?php
          }
          // If there weren't enough entries into this competition fill out with blanks.
          for ($i = $entry_no; $i < $max_entries[$comp_no]; $i++) {
            ?><td>&#160;</td><?php
          }
        }
        else {
          // No scores for this column.
          for ($i = 0; $i < $max_entries[$comp_no]; $i++) {
            ?><td>&#160;</td><?php
          }
        }
        ?><td class="scoreTotal"><?php print $entrant_score->getTotal() ?></td><?php
        $comp_no++;
      }
      // If the entrant had no entries for the final competition in the series
      // this fill in to the end of the row with blanks.
      while ($comp_no < $num_comps) {
        for ($i = 0; $i < $max_entries[$comp_no]; $i++) {
          ?><td>&#160;</td><?php
        }
        ?><td>&#160;</td><?php
        $comp_no++;
      }
      if ($row == $num_rows - 1):
      ?>
      <td class="scoreTotal"><?php print $super_entrant_score->getTotal() ?></td>
<?php else: ?>
      <td>&nbsp;</td>
<?php endif; ?>
    </tr>    
<?php
      $row++;
    endforeach;
  endforeach; ?>
    </tbody>
  </table>
<p class="csvdownload">[<?php print l(t("Download as a CSV file"), $csvurl); ?>]</p>
<?php endif;
