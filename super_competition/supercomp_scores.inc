<?php
/**
 * @file
 *
 * Collect a lot of scores!
 */

/**
 * Brings together a number of cumulative competitions into one grand super competition.
 */
class SuperCompScores {
  private $super_entrant_scores = array(); // An array of SuperEntrantScores object
  private $cumulative_competitions = array(); // Array of CumuCompScores objects
  private $num_comps = 0;
  
  /**
   * Find all of the cumulative competitions which are a part of this super
   * competition and add them to this object.
   * 
   * @param $nid
   * Node id of the super competition node
   */
  function __construct($nid) {
    $result = db_query('SELECT nid, scoring_scheme FROM {cumulative_competition} WHERE super_competition = %d', $nid);
    while ($row = db_fetch_object($result)) {
      $cumucomp = new CumuCompScores($row->nid, $row->scoring_scheme);
      $this->addCumuCompScores($row->nid, $cumucomp);
    }
  }

  /*
   * Extract scores from the cumulative competitions and keep a list of those competitions.
   * 
   * @param $nid
   * Node id of the super competition node.
   * 
   * @param $cumucomp
   * A CumuCompScores object.
   */
  private function addCumuCompScores($nid, $cumucomp) {
    // Add this cumulative competition to this list of competitions.
    $this->cumulative_competitions[] = $cumucomp;
    foreach ($cumucomp->getCumuEntrantScores() as $cumu_entrant_score) {
      $author = $cumu_entrant_score->getAuthor();
      if (!array_key_exists($author, $this->super_entrant_scores)) {
        $this->super_entrant_scores[$author] = new SuperEntrantScores($author);
      }
      $this->super_entrant_scores[$author]->addCumuEntrantScores($cumucomp, $cumu_entrant_score);
    }
    if ($cumucomp->getNumCompetitions() > $this->num_comps) {
      $this->num_comps = $cumucomp->getNumCompetitions();
    } 
    uasort(&$this->super_entrant_scores, "cumucomp_compare_entrant_totals");
  }
  
  function getCumulativeCompetitions() {
    return $this->cumulative_competitions;
  }
  
  function getSuperEntrantScores() {
    $cur_pos = 0;
    $count = 0;
    $prev_score = 1000;
    foreach ($this->super_entrant_scores as $super_entrant_scores) {
      $count++;
      $total = $super_entrant_scores->getTotal();
      if ($total < $prev_score) {
        $cur_pos = $count;
        $prev_score = $total;
      }
      $super_entrant_scores->position = $cur_pos;
    }
    return $this->super_entrant_scores;
  }
  
  function getNumCompetitions() {
    return $this->num_comps;
  }
    
  function getCompetitionList() {
    $result = array();
    foreach ($this->super_entrant_scores as $super_entrant_scores) {
      $cumucomps = $super_entrant_scores->getCumuComps();
      foreach ($cumucomps as $cumucomp) {
        $complist = $cumucomp->getCompetitionList();
        foreach ($complist as $comp) {
          if (!array_key_exists($comp->getCompetitionId(), $result)) {
            $result[$comp->getCompetitionId()] = $comp;
          }
        }
      }
    }
    return $result;
  }
  
  /**
   * Build a list of competition info for each cumulative competition which has
   * been added. Each list will contain the maximum number of entries and the
   * competition id.
   */
  function getCompetitionInfo() {
    $result = array();
    $num_comps = 0;
    foreach ($this->cumulative_competitions as $cumucomp) {
      $comp_no = 0;
      $comp_max_entries = array();
      $comp_ids = array();
      foreach ($cumucomp->getCompetitionList() as $competition) {
        $comp_max_entries[$comp_no] = $competition->getMaxEntries();
        $comp_ids[] = $competition->getCompetitionId(); 
        $comp_no++;
      }
      // Cumulative competitions have an id.
      $result[$cumucomp->getCompetitionId()] = array('comp_max_entries' => $comp_max_entries, 'comp_ids' => $comp_ids);
      if ($comp_no > $num_comps) {
        $num_comps = $comp_no;
      }
    }
    // Now find the maximum of the comp_max_entries for all of the cumulative competitions.
    $super_max_entries = array();
    for ($i = 0; $i < $num_comps; $i++) {
      $super_max_entries[$i] = 0;
    }
    foreach ($result as $compdata) {
      $i = 0;
      foreach ($compdata['comp_max_entries'] as $max) {
        if ($max > $super_max_entries[$i]) {
          $super_max_entries[$i] = $max;
        }
        $i++;
      }
    }
    $result['max'] = $super_max_entries;
    $result['numcomps'] = $num_comps;
    return $result;
  }
}
