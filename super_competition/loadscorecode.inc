<?php
/**
 * @file
 * Load the score calculation code.
 */
module_load_include('inc', 'competition', 'score');
module_load_include('inc', 'competition', 'entrant_scores');
module_load_include('inc', 'competition', 'competition_scores');
module_load_include('inc', 'cumulative_competition', 'cumucomp_scores');
module_load_include('inc', 'cumulative_competition', 'cumu_entrant_scores');
module_load_include('inc', 'super_competition', 'supercomp_scores');
module_load_include('inc', 'super_competition', 'super_entrant_scores');
