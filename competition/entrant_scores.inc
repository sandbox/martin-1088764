<?php
/**
 * @file
 * Various classes for keeping track of an entrants scores.
 */

/**
 * An entrant's scores in a single competition. Typically it would contain three scores.
 */
class EntrantScores {
  private $scores = array();
  private $author;
  public $position;
  public $first;
  public $last;
  private $competition_scores;

  /**
   * EntrantScores constructor.
   *
   * @param $competition_scores
   * The CompetitionScores object.
   *
   * @param $author
   * The author's name.
   */
  function __construct($competition_scores, $author) {
    $this->competition_scores = $competition_scores;
    $this->author = $author;
  }

  /**
   * Add a single score.
   *
   * @param $score
   * The score which is a Score object.
   */
  function addScore($score) {
    if ($score->author != $this->author) {
      throw new Exception("Invalid author, was " . $score->author . " should be " . $this->author);
    }
    $this->scores[] = $score;
  }

  /**
   * Calculates the total for the score. As a side effect it also markes which score
   * is first in the list and which is last.
   */
  function getTotal() {
    $result = 0;
    $current = NULL;
    foreach ($this->scores as $score) {
      $result += $score->score;
      $current = $score;
      $current->first = FALSE;
      $current->last = FALSE;
    }
    $this->scores[0]->first = TRUE;
    $current->last = TRUE;
    return $result;
  }

  /**
   * Get the author's user id.
   */
  function getUid() {
    return $this->scores[0]->uid;
  }

  /**
   * Get the author's name.
   */
  function getAuthor() {
    return $this->author;
  }

  /**
   * Safe version of author.
   */
  function getPlainAuthor() {
    return check_plain($this->author);
  }

  /**
   * Get the array of Score objects.
   */
  function getScores() {
    return $this->scores;
  }

  /**
   * Get the number of scores in this entry.
   */
  function getNumEntries() {
    return count($this->scores);
  }

  /**
   * Get the id of the competition_scores.
   */
  function getCompetitionId() {
    return $this->competition_scores->getCompetitionId();
  }

  /**
   * Get the competition_scores from which these scores come.
   */
  function getCompetitionScores() {
    return $this->competition_scores;
  }
}

