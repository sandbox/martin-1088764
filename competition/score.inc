<?php
/**
 * @file
 * Contains the implementation of the Score class.
 */

 /**
  * A single score in a competition. Contains everything one
  * could ever wish to know about that score.
  */
class Score {
  public $sid; // Score id.
  public $uid;
  public $author;
  public $title;
  public $score;
  public $best = FALSE;
  public $first = FALSE; /* Is the first score in a list. */
  public $last = FALSE; /* Is the last score in a list. */

  /**
   * Return a CSS class for this score.
   */
  public function getClass() {
    $result = "";
    if ($this->score == 20) {
      $result = "scoreTwenty";
    }
    elseif ($this->score == 19) {
      $result = "scoreNineteen";
    }
    elseif ($this->score == 18) {
      $result = "scoreEighteen";
    }
    else {
      $result = "normal_score";
    }
    if ($this->best) {
      $result .= " scoreBest";
    }
    return $result;
  }

  /**
   * Safe version of title.
   */
  function getPlainTitle() {
    return check_plain($this->title);
  }

  /**
   * Safe version of the score.
   */
  function getPlainScore() {
    return check_plain($this->score);
  }
}
