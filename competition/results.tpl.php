<?php
/**
 * @file
 * Output competition results.
 */
 ?>

  <div class="scores">
    <table>
      <?php foreach ($scores as $entrant_score): ?>
        <?php foreach ($entrant_score->getScores() as $score): ?>
        <?php
               if ($score->first) { $showpos = TRUE; $showauthor = TRUE; } else { $showpos = FALSE; $showauthor = FALSE; }
               if ($score->last) { $showtotal = TRUE; } else { $showtotal = false; }
        ?>
      <tr>
        <td><?php if ($showpos) { print $entrant_score->position; } else { print '&nbsp;'; } ?></td>
        <td><?php if ($showauthor) { print $entrant_score->getPlainAuthor(); } else { print '&nbsp;'; } ?></td>
        <td><?php print $score->getPlainTitle() ?><?php if ($score->best == 1) { print ' (Best Entry)';} ?></td>
        <td class="<?php print $score->getClass() ?>"><?php print $score->getPlainScore() ?></td>
  <?php if ($canedit): ?>
        <td>
          <a href="<?php print '/node/' . $nid . '/editscore/' . $score->sid ?>"><img src="/sites/all/themes/storrcam/button_edit.png" /></a>
          <a href="<?php print '/node/' . $nid . '/deletescore/' . $score->sid ?>"><img src="/sites/all/themes/storrcam/button_trash.png" /></a>
        </td>
  <?php endif; ?>
        <td class="scoreTotal"><?php if ($showtotal) { print $entrant_score->getTotal(); } else { print '&nbsp;'; } ?></td>
      </tr>
        <?php endforeach; ?>
      <?php endforeach; ?>
    </table>
  </div>
