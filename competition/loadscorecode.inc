<?php
/**
 * @file
 * Load the score objects which are needed when calculating results
 */
module_load_include('inc', 'competition', 'score');
module_load_include('inc', 'competition', 'entrant_scores');
module_load_include('inc', 'competition', 'competition_scores');
// Uses the cumulative competition objects to find out which cumulative competition this competition belongs to.
module_load_include('inc', 'cumulative_competition', 'cumucomp_scores');
module_load_include('inc', 'cumulative_competition', 'cumu_entrant_scores');
