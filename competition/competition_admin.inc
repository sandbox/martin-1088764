<?php

/**
 * Implements node_admin().
 */
function competition_admin() {
  $form = array();
  $profile_fields = competition_admin_get_profile_field_names();
  $form['competition_firstname'] = array(
      '#type' => 'select',
      '#title' => t('User\'s first name profile field'),
      '#default_value' => variable_get('competition_firstname'),
      '#options' => $profile_fields,
      '#description' => t('Choose the profile field which contains the user\'s first name or choose None if there is no profile field.')
  );
  $form['competition_lastname'] = array(
      '#type' => 'select',
      '#title' => t('User\'s last name profile field'),
      '#default_value' => variable_get('competition_lastname'),
      '#options' => $profile_fields,
      '#description' => t('Choose the profile field which contains the user\'s last name or choose None if there is no profile field.')
  );
  return system_settings_form($form);
}

/**
 * Get a list of profile field names.
 */
function competition_admin_get_profile_field_names() {
    $result = array();
    $query = db_query('SELECT title, name FROM {profile_fields} ORDER BY weight ASC');
    while ($row = db_fetch_object($query)) {
        $result[$row->name] = $row->title;
    }
    return $result;
}