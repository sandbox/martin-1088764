<?php
/**
 * @file
 * Output information about the competition.
 */
 ?>

  <div class="type-of-node">
    <span class="date"><?php print $date['day'] ?>/<?php print $date['month'] ?>/<?php print $date['year'] ?></span>
    <span class="comptitle"><?php print $comptitle ?></span>
    <span class="judge"><?php print $judge ?></span>
  </div>
