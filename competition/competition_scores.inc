<?php
/**
 * @file
 * Object for handling a competition. This represents and actual event
 * like a print competition evening.
 */

function compare_totals($a, $b) {
  $a_total = $a->getTotal();
  $b_total = $b->getTotal();
  if ($a_total > $b_total) {
    return -1;
  }
  elseif ($a_total == $b_total) {
    return 0;
  }
  else {
    return 1;
  }
}

/**
 * Scores for a competition. This represents a real event
 * where people score points. These can be agregated into
 * cumulative competitions.
 */
class CompetitionScores {
  private $entrant_scores = array(); // Array of EntrantScores objects (1 per entrant).
  private $competition_id = 0; // A unique id for this competition.
  private $competition_name;
  private $judge;
  private $date;

  /**
   * Construct a CompetitionScores object.
   *
   * @param $nid
   * The node id of the competition. This must be a competition node.
   */
  function __construct($nid) {
    $this->competition_id = $nid;
    $compdata = db_fetch_object(db_query(db_rewrite_sql('SELECT title, competition_date, judge from {node} n INNER JOIN {competition} c ON c.nid = n.nid WHERE n.nid = %d'), $nid));
    $this->judge = $compdata->judge;
    $date = date_convert($compdata->competition_date, DATE_UNIX, DATE_ARRAY);
    $this->date = (drupal_strlen($date['day']) == 1 ? '0' . $date['day'] : $date['day']) . '/' . (drupal_strlen($date['month']) == 1 ? '0' . $date['month'] : $date['month']) . '/' . $date['year'];
    $this->competition_name = $compdata->title;
    // Get the scores from the database.
    $result = db_query('SELECT sid, uid, name, title, score, best FROM {score} WHERE nid = %d ORDER BY name', $nid);
    $entrant_data = array();
    while ($row = db_fetch_array($result)) {
      $score = new Score();
      $score->sid = $row['sid'];
      $score->uid = $row['uid'];
      $score->author = $row['name'];
      $score->title = $row['title'];
      $score->score = $row['score'];
      $score->best = $row['best'];

      // If there isn't and EntrantScores object for this author then make one and add it
      // to the list.
      if (!array_key_exists($score->author, $entrant_data)) {
        $entrant_data[$score->author] = new EntrantScores($this, $score->author);
        // Add this score to the authors tally.
        $entrant_data[$score->author]->addScore($score);
        $this->addEntrant($entrant_data[$score->author]);
      }
      else {
        // Add this score to the authors tally.
        $entrant_data[$score->author]->addScore($score);
      }
    }
    uasort(&$this->entrant_scores, "compare_totals"); // Sort the scores.
    $this->calcPositions();
  }

  /**
   * Get the date in formatted form.
   */
  function getDate() {
    return $this->date;
  }

  /**
   * Get the name of the judge.
   */
  function getJudge() {
    return $this->judge;
  }

  /**
   * Get a safe version of the judges name.
   */
  function getPlainJudge() {
    return check_plain($this->judge);
  }

  /**
   * Return the name of the competition.
   */
  function getCompetitionName() {
    return $this->competition_name;
  }

  /**
   * Return a safe version of the competition name.
   */
  function getPlainCompetitionName() {
    return check_plain($this->competition_name);
  }

  /**
   * Add an entrants scores for this competition (usually up to three scores).
   *
   * @param $entrant_scores
   * An EntrantScores object to be added to the entrant_scores private
   * member array.
   */
  private function addEntrant($entrant_scores) {
    $this->entrant_scores[] = $entrant_scores;
  }

  /**
   * Called when retrieving the scores for display. All of
   * the scores have been added to the EntrantScores so
   * this can work out the author's position.
   */
  private function calcPositions() {
    $cur_pos = 0;
    $count = 0;
    $prev_score = 1000;
    foreach ($this->entrant_scores as $score) {
      $count++;
      $total = $score->getTotal();
      if ($total < $prev_score) {
        $cur_pos = $count;
        $prev_score = $total;
      }
      $score->position = $cur_pos;
    }
  }

  function getCompetitionId() {
    return $this->competition_id;
  }

  /**
   * This gets the entrant score data.
   *
   * @return
   * An array of EntrantScores objects.
   */
  function getEntrantScores() {
    return $this->entrant_scores;
  }

  /**
   * Calculate the maximum number of entries from authors.
   *
   * @return
   * The maxiumum number of entries from a single author. These days it's three, it
   * has been two for one season.
   */
  function getMaxEntries() {
    $result = 0;
    foreach ($this->entrant_scores as $entrant_scores) {
      if ($entrant_scores->getNumEntries() > $result) {
        $result = $entrant_scores->getNumEntries();
      }
    }
    return $result;
  }
}
