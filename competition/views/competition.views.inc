<?php
/**
 * @file
 *
 * Implement views integration for competitions.
 */

/**
 * Implements hook_views_data().
 */
function competition_views_data() {
  // Prefix in UI to make it easier to find the field.
  $data['competition']['table']['group'] = t('Competitions');

  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['competition']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['competition']['competition_date'] = array(
    'title' => t('Competition date'),
    'help' => t('The date this competition took place.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['competition']['judge'] = array(
    'title' => t('Judge'),
    'help' => t('The judge of this competition entries.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['competition']['cumulative_competition'] = array(
    'title' => t('Cumulative Competition'),
    'help' => t('The Cumulative Competition which this competition is a part of.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Cumulative Competition'),
    ),
  );

  // Prefix in UI to make it easier to find the field.
  $data['score']['table']['group'] = t('Scores');
  // Make this a base table.
  $data['score']['table']['base'] = array(
    'field' => 'sid',
    'title' => 'Score',
    'help' => t('Competition scores.')
  );

  $data['score']['nid'] = array(
    'title' => t('Competition'),
    'help' => t('The Competition which this score is from.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Competition'),
    ),
  );
  $data['score']['name'] = array(
    'title' => t('The author\'s name'),
    'help' => t('The full name of the entrant.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['score']['title'] = array(
    'title' => t('Entry title'),
    'help' => t('The title of the entered image'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['score']['score'] = array(
    'title' => t('Score'),
    'help' => t('The score for this entry.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  return $data;
}