<?php
/**
 * @file
 * Load the score objects required for calculating results.
 */
module_load_include('inc', 'competition', 'score');
module_load_include('inc', 'competition', 'entrant_scores');
module_load_include('inc', 'competition', 'competition_scores');
module_load_include('inc', 'cumulative_competition', 'cumucomp_scores');
module_load_include('inc', 'cumulative_competition', 'cumu_entrant_scores');
