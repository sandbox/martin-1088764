<?php
/**
 * @file
 * An entrants scores in cumulative competitions.
 */

/**
 * An author's scores for a number of competitions which are being accumulated.
 */

class CumuEntrantScores {
  private $entrant_scores = array();
  private $author;
  private $uid;
  private $competition_scores;
  private $cumu_comp_scores; // A CumuCompScores or CumuCompIndividualScores object
  public $position;
  private $scoring_scheme;
  
  /**
   * Make a CumuEntrantScores object for a given author.
   * 
   * @param $cumu_comp_scores
   * The parent CumuCompScores or CumuCompIndividualScores object.
   * 
   * @param $author
   * The author's name.
   * 
   * @param $competition_scores
   * A CompetitionScores object.
   */
  function __construct($cumu_comp_scores, $author, $competition_scores, $scoring_scheme) {
    $this->cumu_comp_scores = $cumu_comp_scores;
    $this->author = $author;
    $this->competition_scores = $competition_scores;
    $this->scoring_scheme = $scoring_scheme;
  }
  
  /**
   * Add the scores from a competition_scores to this accumulator.
   * 
   * @param $entrant_scores
   * An EntrantScores object containing the scores from a competition
   * for this author.
   */
  function addEntrantScores($entrant_scores) {
    if ($entrant_scores->getAuthor() != $this->author) {
      throw new Exception("Invalid author, was " . $entrant_scores->getAuthor() . " should be " . $this->author);
    }
    $this->uid = $entrant_scores->getUid();
    $this->entrant_scores[] = $entrant_scores;
  }
  
  /**
   * Get the accumulated total for this author.
   * 
   * As a side effect this also sets first and last flags on the EntrantScores objects.
   */
  function getTotalCumulative() {
    $result = 0;
    $current = NULL;
    foreach ($this->entrant_scores as $entrant_score) {
      $entrant_score->first = $entrant_score->last = FALSE;
      $result += $entrant_score->getTotal();
      $current = $entrant_score;
    }
    $this->entrant_scores[0]->first = TRUE;
    $current->last = TRUE;
    return $result;
  }

  function getTotalCumulativeTop9() {
    $result = 0;
    $current = NULL;
    $x = array();
    foreach ($this->entrant_scores as $entrant_score) {
      $scores = $entrant_score->getScores();
      foreach ($scores as $score) {
        $x[] = $score->score;
      }
      $entrant_score->first = $entrant_score->last = FALSE;
      $current = $entrant_score;
    }
    // Add up the highest 9 results.
    sort($x);
    $x = array_reverse($x);
    $count = 9;
    if (count($x) < $count) {
      $count = count($x);
    }
    $i = 0;
    while ($count > 0) {
      $result += $x[$i++];
      $count--;
    }
    $this->entrant_scores[0]->first = TRUE;
    $current->last = TRUE;
    return $result;
  }

  function getTotal() {
    if ($this->scoring_scheme == 'cumulative') {
      return $this->getTotalCumulative();
    }
    elseif ($this->scoring_scheme == 'top-nine') {
      return $this->getTotalCumulativeTop9();
    }
    else {
      return 0;
    }
  }
  
  /**
   * Get the author's name.
   */
  function getAuthor() {
    return $this->author;
  }

  /**
   * Safe version of author.
   */
  function getPlainAuthor() {
    return check_plain($this->author);
  }
  
  /**
   * Get the user's id
   */
  function getUid() {
    return $this->uid;
  }
  
  /**
   * Get the list of EntrantScores.
   */
  function getEntrantScores() {
    return $this->entrant_scores;
  }
  
  /**
   * Get the number of competition scores which are being accumulated.
   */
  function getNumEntrantScores() {
    return count($this->entrant_scores);
  }
  
  /**
   * Get the maximum number of scores in the competitions which make up
   * this accumulator.
   */
  function getMaxEntries() {
    $result = 0;
    foreach ($this->entrant_scores as $entrant_score) {
      if ($entrant_score->getNumEntries() > $result) {
        $result = $entrant_score->getNumEntries();
      }
    }
    return $result;
  }

  /**
   * Get the competition.
   */
  function getCompetitionScores() {
    return $this->competition_scores;
  }
}

