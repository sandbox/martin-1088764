<?php
/**
 * @file
 *
 * Accumulated score data.
 */

function cumucomp_compare_entrant_totals($a, $b) {
  $a_total = $a->getTotal();
  $b_total = $b->getTotal();
  if ($a_total > $b_total) {
    return -1;
  }
  elseif ($a_total == $b_total) {
    return 0;
  }
  else {
    return 1;
  }
}

/**
 * All of the competition scores for a cumulative competition
 * like printer of the year.
 */
class CumuCompScores {
  private $cumu_entrant_scores = array();
  private $competition_scores_list = array();
  private $num_comps = 0;
  private $comp_id = -1;
  private $scoring_scheme = 'cumulative';

  /**
   * Build it from the node id of the cumulative competition.
   */
  function __construct($nid, $scoring_scheme) {
    // TODO: Support versioning.
    $this->comp_id = $nid;
    $this->scoring_scheme = $scoring_scheme;
    $result = db_query('SELECT nid FROM {competition} WHERE cumulative_competition = %d', $nid);
    while ($row = db_fetch_object($result)) {
      $comp_scores = new CompetitionScores($row->nid, $scoring_scheme);
      $this->addCompetitionScores($comp_scores);
    }
    uasort(&$this->cumu_entrant_scores, "cumucomp_compare_entrant_totals");
  }

  /**
   * Extract the scores from a CompetitionScores object and add
   * CumuEntrantScores objects for each author to the
   * cumu_entrant_scores member variable. Add each competition score
   * to that author's entry in cumu_entrant_scores.
   *
   * @param $comp_scores
   * A CompetitionScores object containing the scores for that competition.
   */
  private function addCompetitionScores($comp_scores) {
    $this->competition_scores_list[] = $comp_scores;
    foreach ($comp_scores->getEntrantScores() as $entrant_scores) {
      $author = $entrant_scores->getAuthor();
      if (!array_key_exists($author, $this->cumu_entrant_scores)) {
        $this->cumu_entrant_scores[$author] = new CumuEntrantScores($this, $author, $comp_scores, $this->scoring_scheme); // comp_scores is a CompetitionScores object.
      }
      $this->cumu_entrant_scores[$author]->addEntrantScores($entrant_scores);
      $num_comps = $this->cumu_entrant_scores[$author]->getNumEntrantScores();
      if ($num_comps > $this->num_comps) {
        $this->num_comps = $num_comps;
      }
    }
  }

  function getCompetitionList() {
    return $this->competition_scores_list;
  }

  /**
   * This doesn't just return the cumu_entrant_scores member variable, it
   * also goes through the array of entrant's scores and adds the total score
   * and its position in the competition.
   */
  function getEntrantScores() {
    $cur_pos = 0;
    $count = 0;
    $prev_score = 1000;
    foreach ($this->cumu_entrant_scores as $entrant_scores) {
      $count++;
      $total = $entrant_scores->getTotal();
      if ($total < $prev_score) {
        $cur_pos = $count;
        $prev_score = $total;
      }
      $entrant_scores->position = $cur_pos;
    }
    return $this->cumu_entrant_scores;
  }

  /**
   * Get the cumulative scores for this competition.
   */
  function getCumuEntrantScores() {
    return $this->cumu_entrant_scores;
  }

  function getCompetitionId() {
    return $this->comp_id;
  }

  /**
   * The number of competitions in this accumulaton of competitions.
   */
  function getNumCompetitions() {
    return $this->num_comps;
  }

  function getScoringScheme() {
    return $this->scoring_scheme;
  }
}

class CumuCompIndividualScores {
  private $cumu_entrant_scores = array();
  private $competition_scores_list = array();
  private $num_comps = 0;
  private $comp_id = -1;
  private $scoring_scheme = 'cumulative';

  /*
   * Build it from the node id of the cumulative competition.
   */
  function __construct($nid, $uid, $scoring_scheme) {
    // TODO: Support versioning.
    $this->scoring_scheme = $scoring_scheme;
    $result = db_query('SELECT nid FROM {competition} WHERE cumulative_competition = %d', $nid);
    while ($row = db_fetch_object($result)) {
      $comp_scores = new CompetitionScores($row->nid, $scoring_scheme);
      $this->addCompetitionScores($row->nid, $comp_scores, $uid);
    }
  }

  /*
   * Extract the scores from a CompetitionScores object and add
   * CumuEntrantScores objects for each author to the
   * cumu_entrant_scores member variable. Add each competition score
   * to that author's entry in cumu_entrant_scores.
   *
   * @param $comp_id
   * A unique id for the competition.
   *
   * @param $comp_scores
   * A CompetitionScores object containing the scores for that competition.
   */
  private function addCompetitionScores($comp_id, $comp_scores, $uid) {
    $this->comp_id = $comp_id;
    $this->competition_scores_list[] = $comp_scores;
    foreach ($comp_scores->getEntrantScores() as $entrant_scores) {
      if ($entrant_scores->getUid() == $uid) {
        $author = $entrant_scores->getAuthor();
        if (!array_key_exists($author, $this->cumu_entrant_scores)) {
          $this->cumu_entrant_scores[$author] = new CumuEntrantScores($this, $author, $comp_scores, $this->scoring_scheme);
        }
        $this->cumu_entrant_scores[$author]->addEntrantScores($entrant_scores);
        $num_comps = $this->cumu_entrant_scores[$author]->getNumEntrantScores();
        if ($num_comps > $this->num_comps) {
          $this->num_comps = $num_comps;
        }
      }
    }
  }

  /**
   * An identifier for this cumulative competition, not the individual ones inside it.
   */
  public function getCompetitionId() {
    return $this->comp_id;
  }

  function getCompetitionList() {
    return $this->competition_scores_list;
  }

  /*
   * This doesn't just return the cumu_entrant_scores member variable, it
   * also goes through the array of entrant's scores and adds the total score
   * and its position in the competition.
   */
  function getEntrantScores() {
    return $this->cumu_entrant_scores;
  }

  /*
   * The number of competitions in this accumulaton of competitions.
   */
  function getNumCompetitions() {
    return $this->num_comps;
  }
}
