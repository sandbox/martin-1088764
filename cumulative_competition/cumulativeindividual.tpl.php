<?php
/**
 * @file
 * Show an individuals scores in a series of competitions.
 */
?>
<h2><?php print $users_name ?></h2>
  <table class="scoresheet">
    <tbody>
  <?php
      foreach ($cumu_scores->getEntrantScores() as $entrant_scores):
        if ($entrant_scores->getUid() == $uid) {
        $comp_id = -1; // Impossible value.
        foreach ($entrant_scores->getEntrantScores() as $entrant_score) {
          $competition = $entrant_score->getCompetitionScores();
          if ($competition->getCompetitionId() != $comp_id) {
   ?>
      <tr>
        <td class="comptitle" colspan="2"><?php print $competition->getDate() . ': ' . $competition->getPlainCompetitionName() . ' - ' . $competition->getPlainJudge() ?></td>
      </tr>
  <?php
          }
          foreach ($entrant_score->getScores() as $score) {
            $entry_no++; 
            ?><tr><td><?php print $score->getPlainTitle() ?></td><td class="<?php print $score->getClass() ?>""><?php print $score->getPlainScore(); ?></td></tr><?php
          }
        }
      }
    endforeach; ?>
  </tbody>
  </table>
