<?php
/**
 * @file
 *
 * Views integration for cumulative competitions.
 */

/**
 * Implements hook_views_data().
 */
function cumulative_competition_views_data() {
  // Prefix in UI to make it easier to find the field.
  $data['cumulative_competition']['table']['group'] = t('Competitions');

  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['cumulative_competition']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['cumulative_competition']['season'] = array(
    'title' => t('Cumulative Competition Season'),
    'help' => t('The season the Cumulative Competition took place.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Season'),
    ),
  );

  $data['cumulative_competition']['competition_type'] = array(
    'title' => t('Cumulative Competition Type'),
    'help' => t('The type of (e.g., Monthly) Cumulative Competition.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Competition Type'),
    ),
  );

  $data['cumulative_competition']['entry_type'] = array(
    'title' => t('Cumulative Competition Entry Type'),
    'help' => t('The type of entry (e.g., Print) for the Cumulative Competition.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Entry Type'),
    ),
  );

  $data['cumulative_competition']['super_competition'] = array(
    'title' => t('Super Competition'),
    'help' => t('Cumulative Competition\'s parent super accumulator.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Super Competition'),
    ),
  );

  $data['cumulative_competition']['scoring_scheme'] = array(
    'title' => t('Scoring Scheme'),
    'help' => t('The way the scores are used to calculate the winner.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}