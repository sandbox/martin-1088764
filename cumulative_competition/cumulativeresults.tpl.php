<?php
/**
 * @file
 * Show a cumulative set of results.
 */
?>
<?php if ($scoring_scheme == 'top-nine'): ?>
<p><em>Note: only the top nine scores are included in the total.</em></p>
<?php endif; ?>
  <table class="scoresheet">
    <thead>
    <tr>
      <th>&nbsp;</th>
      <th>Name</th>
<?php
      $comp_no = 0;
      $comp_max_entries = array();
      $comp_ids = array();
      foreach ($cumu_scores->getCompetitionList() as $competition) {
        $comp_max_entries[$comp_no] = $competition->getMaxEntries();
        $comp_ids[] = $competition->getCompetitionId(); 
        ?><th class="compno" title="<?php print $competition->getPlainCompetitionName() . ' - ' . $competition->getPlainJudge() ?>" colspan="<?php print $comp_max_entries[$comp_no] + 1 ?>"><?php print l($comp_no + 1, 'node/' . $competition->getCompetitionId()); ?></th><?php
        $comp_no++;
      } ?>
      <th>Total</th>
    </tr>
    </thead>
    <tbody>
  <?php foreach ($cumu_scores->getEntrantScores() as $entrant_scores): ?>
    <tr>
      <td><?php print $entrant_scores->position ?></td>
      <td><?php print l($entrant_scores->getAuthor(), "node/" . $nid . "/user/" . $entrant_scores->getUid()); ?></td>
<?php
      $comp_no = 0;
      foreach ($entrant_scores->getEntrantScores() as $entrant_score) {
        $entry_no = 0;
        while ($entrant_score->getCompetitionId() != $comp_ids[$comp_no]) {
          for ($i = $entry_no; $i <= $comp_max_entries[$comp_no]; $i++) {
            ?><td>&nbsp;</td><?php
          }
          $comp_no++;
        }
        if ($entrant_score->getCompetitionId() == $comp_ids[$comp_no]) {
          foreach ($entrant_score->getScores() as $score) {
            $entry_no++; 
            ?><td class="<?php print $score->getClass() ?>" title="<?php print $score->getPlainTitle(); ?>"><?php print $score->getPlainScore(); ?></td><?php
          }
          for ($i = $entry_no; $i < $comp_max_entries[$comp_no]; $i++) {
            ?><td>&nbsp;</td><?php
          }
        }
        else {
          for ($i = 0; $i < $comp_max_entries[$comp_nod]; $i++) {
            ?><td>&nbsp;</td><?php
          }
        }
        ?><td class="scoreTotal"><?php print $entrant_score->getTotal() ?></td><?php
        $comp_no++;
      }
      while ($comp_no < count($comp_ids)) {
        for ($i = 0; $i <= $comp_max_entries[$comp_no]; $i++) {
          ?><td>&nbsp;</td><?php
        }        
        $comp_no++;
      } 
      ?>
      <td class="scoreTotal"><?php print $entrant_scores->getTotal() ?></td>
    </tr>      
  <?php endforeach; ?>
  </tbody>
  </table>
<p class="csvdownload">[<?php print l(t('Download as a CSV file'), $csvurl); ?>]</p>